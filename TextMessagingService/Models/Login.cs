﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TextMessagingService.Models
{
    public class Login
    {
        [Key]
        public int U_id { get; set; }

        public string U_name { get; set; }
        [Required]

        public string U_pw { get; set; }
        [Required]

        public int U_pn { get; set; }


    }
}
