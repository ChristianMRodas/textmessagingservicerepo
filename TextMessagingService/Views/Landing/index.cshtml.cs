﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TextMessagingService.Models;

namespace TextMessagingService.Views.Landing
{
    public class LandingModel : PageModel
    {
        private ApplicationDbContext _db;

        public LandingModel(ApplicationDbContext db)
        {
            _db = db;
        }

        [BindProperty]
        public Login Login{ get; set; }

        
        public async Task<IActionResult> OnGet(int? U_id)
        {
            Login = new Login();
            if (U_id == null)
            {
                //create
                return Page();
            }

            //update
            Login = await _db.Login.FirstOrDefaultAsync(u => u.U_id == U_id);
            if (Login == null)
            {
                return NotFound();
            }
            return Page();

        }
        

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {

                if (Login.U_id == 0)
                {
                    _db.Login.Add(Login);
                }
                else
                {
                    _db.Login.Update(Login);
                }

                await _db.SaveChangesAsync();

                return RedirectToPage("Index");
            }
            return RedirectToPage();
        }




    }
}
