﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TextMessagingService.Migrations
{
    public partial class TextMessagingService : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Login",
                columns: table => new
                {
                    U_name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    U_pw = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    U_pn = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Login", x => x.U_name);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Login");
        }
    }
}
