﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TextMessagingService.Models;

namespace TextMessagingService.Controllers
{
    [Route("api/Login")]
    [ApiController]
    public class LandingController : Controller
    {

        private readonly ApplicationDbContext _db;

        
        public LandingController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Json(new { data = await _db.Login.ToListAsync() });
        }



        [HttpDelete]
        public async Task<IActionResult> Delete(int U_id)
        {
            var bookFromDb = await _db.Login.FirstOrDefaultAsync(u => u.U_id == U_id);

            if (bookFromDb == null)
            {
                return Json(new { success = false, message = "Error while Deleting" });
            }

            _db.Login.Remove(bookFromDb);
            await _db.SaveChangesAsync();
            return Json(new { success = true, message = "Delete successful" });
        }
        
    }
}
